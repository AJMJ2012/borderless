; ---------------------------------------------------------------- ;
; Borderless, Center and Resize script by Dark-Assassin            ;
; ---------------------------------------------------------------- ;
; Alt + 1: Removes window decoration                               ;
; Alt + 2: Center window                                           ;
; Alt + 3: Resize window                                           ;
; Alt + `: All of the above at once                                ;
; ---------------------------------------------------------------- ;
; Note: ` may also be seen as ~ and but not '                      ;
; ---------------------------------------------------------------- ;

LALT & 1::Borderless()
Borderless() {
	SetTitleMatchMode, 2
	WinGet Style, Style, A
	if(Style & 0xC40000) {
		WinSet, Style, -0xC40000, A
		WinSet, Style, -0xC00000, A
	} else {
		WinSet, Style, +0xC40000, A
		WinSet, Style, +0xC00000, A
	}
	return
}

LALT & 2::Center()
Center() {
	SetTitleMatchMode, 2
	WinGet Style, Style, A
    WinGetTitle, windowName, A
    WinGetPos,,, Width, Height, %windowName%
    WinMove, %windowName%,, (A_ScreenWidth/2)-(Width/2), (A_ScreenHeight/2)-(Height/2) - 8
	return
}

LALT & 3::Resize()
Resize() {
	SetTitleMatchMode, 2
	WinGet Style, Style, A
	WinMove,A,,0,0,A_ScreenWidth,A_ScreenHeight
	return
}

LALT & `::Fullscreen()
Fullscreen() {
	Borderless()
	Center()
	Resize()
}